#ifndef __CONS_H__
#define __CONS_H__

void	cons_init(void);
void	cons_reinit(void);
void	cons_rx(void);
void	cons_fwd_rx(void);

void    cons_lora_tx(void);

#endif /* __CONS_H__ */
