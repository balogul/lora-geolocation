/* LoRa task */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "osal.h"

#include <hw_timer0.h>
#include <hw_wkup.h>
#include "sys_watchdog.h"
#include <sys_rtc.h>
#include <ad_nvparam.h>

#include "hw/hw.h"
#include "hw/iox.h"
#include "hw/button.h"
#include "hw/cons.h"
#include "hw/led.h"
#include "lora/logger.h"
#include "lora/lora.h"
#include "lora/param.h"
#include "lora/util.h"
#include "sensor/gps.h"

#include "radio.h"
#include "sx1276/sx1276.h"
#include "sx1276/sx1276-board.h"

#define DEBUG
//#define DEBUG_TIME
//#define HELLO
//#define USE_MY_TIMER

#ifdef USE_MY_TIMER
PRIVILEGED_DATA static uint16_t timer_test1;
PRIVILEGED_DATA static uint16_t timer_test2;
PRIVILEGED_DATA static uint32_t timer0_counter;

static void timer0_int_handler(void)
{
    timer0_counter++;
}
#endif

PRIVILEGED_DATA static logger_data_t last_rx_data;

#define RF_FREQUENCY																868500000 // Hz
#define USE_MODEM_LORA
#define TX_OUTPUT_POWER                             14        // dBm

#if defined( USE_MODEM_LORA )

#define LORA_BANDWIDTH                              2         // [0: 125 kHz,
                                                              //  1: 250 kHz,
                                                              //  2: 500 kHz,
                                                              //  3: Reserved]
#define LORA_SPREADING_FACTOR                       10        // [SF7..SF12]
#define LORA_CODINGRATE                             1         // [1: 4/5,
                                                              //  2: 4/6,
                                                              //  3: 4/7,
                                                              //  4: 4/8]
#define LORA_SYMBOL_TIMEOUT                         5         // Symbols
#define LORA_PREAMBLE_LENGTH                        8         // Same for Tx and Rx
#define LORA_FIX_LENGTH_PAYLOAD_ON                  false
#define LORA_IQ_INVERSION_ON                        false

#endif

#define BUFFER_SIZE                                 32 // Define the payload size here

typedef enum
{
	BAL_TEST_NONE 	= 0x00,
	BAL_TEST_7_7_125,
	BAL_TEST_7_12_125,
	BAL_TEST_7_7_500,
	BAL_TEST_7_12_500,
}bal_test_t;

#define BAL_HDR_SIZE	2

typedef enum
{
	BAL_MODE_NONE		= 0x00,
	BAL_MODE_RX,
	BAL_MODE_TX,
	BAL_MODE_ACK,
}bal_mode_t;

static const uint8_t bal_cmd_hdr[BAL_HDR_SIZE] 		= { 0x19, 0x14 };

static const uint8_t bal_dev_tx_hdr[BAL_HDR_SIZE] = { 0x0C, 0xBA };

static const uint8_t bal_dev_rx_hdr[BAL_HDR_SIZE] = { 0xAE, 0x0B };

static const uint32_t ChannelFreqs[MAX_CHANNELS] = {
	863250000,
	863500000,
	863750000,
	864000000,
	865500000,
	868300000,
	869525000,
	869750000,
};

PRIVILEGED_DATA static OS_TASK lora_task_handle;
PRIVILEGED_DATA static OS_TIMER lora_pin_timer;
PRIVILEGED_DATA static OS_TIMER lora_tx_timer;
PRIVILEGED_DATA static OS_TIMER lora_rx_timer;
PRIVILEGED_DATA static OS_TIMER lora_ack_timer;

PRIVILEGED_DATA static uint16_t BufferSize = BUFFER_SIZE;
PRIVILEGED_DATA static uint8_t Buffer[BUFFER_SIZE];

PRIVILEGED_DATA static uint16_t lora_tx_counter = 1;

PRIVILEGED_DATA static uint8_t dev_id = 0;
PRIVILEGED_DATA static bal_mode_t lora_test_mode = BAL_MODE_NONE;
PRIVILEGED_DATA static bal_test_t cur_test = BAL_TEST_7_12_500;
PRIVILEGED_DATA static uint16_t packet_no_to_send = 0;
PRIVILEGED_DATA static uint16_t test_tx_counter = 0;

PRIVILEGED_DATA static uint8_t ack_count = 0;

/*!
 * Radio events function pointer
 */
PRIVILEGED_DATA static RadioEvents_t RadioEvents;

/*!
 * \brief Function to be executed on Radio Tx Done event
 */
void OnTxDone( void );

/*!
 * \brief Function to be executed on Radio Rx Done event
 */
void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr );

#ifdef DEBUG

#ifdef DEBUG_TIME
static void
debug_time(void)
{
	uint32_t	now = rtc_get();

	printf("%lu:%02lu.%03lu+%02lu ", osticks2ms(now) / 60000,
	    osticks2ms(now) / 1000 % 60, osticks2ms(now) % 1000,
	    now - ms2osticks(osticks2ms(now)));
}
#else
#define debug_time()
#endif

#endif

static uint16_t bal_lora_cmd_get(bal_cmd_t cmd)
{
	uint16_t len = BAL_HDR_SIZE;
	memcpy(Buffer, bal_cmd_hdr, sizeof(bal_cmd_hdr));
	Buffer[len++] = cmd;
	switch(cmd){
	case BAL_CMD_LORA_RESET:
		break;
	case BAL_CMD_LORA_RX:
		break;
	case BAL_CMD_LORA_TX:
		break;
	case BAL_CMD_LORA_ACK:
		memcpy(&Buffer[len], bal_dev_rx_hdr, sizeof(bal_dev_rx_hdr));
		len += sizeof(bal_dev_rx_hdr);
		len += param_get(PARAM_DEV_ID, &Buffer[len], BUFFER_SIZE - len);
		break;
	default:
		break;
	}
	return len;
}

static DioIrqHandler **gp_irqHandlers;

static void
ubx_rx_cb(OS_TIMER timer)
{
	OS_TASK_NOTIFY(lora_task_handle, EV_NOTIF_GPS_RX, eSetBits);
}

static void
lora_tx_cb(OS_TIMER timer)
{
	if(lora_tx_counter > 1){
		lora_tx_counter--;
	}
	OS_TASK_NOTIFY(lora_task_handle, EV_NOTIF_LORA_TX, eSetBits);
}

static void
lora_rx_cb(OS_TIMER timer)
{
	OS_TASK_NOTIFY(lora_task_handle, EV_NOTIF_LORA_RX, eSetBits);
}

static void
lora_ack_cb(OS_TIMER timer)
{
	OS_TASK_NOTIFY(lora_task_handle, EV_NOTIF_LORA_ACK, eSetBits);
}

void
lora_init(void* irq)
{
	gp_irqHandlers = irq;

  Radio.SetChannel( RF_FREQUENCY );

  Radio.SetTxConfig( MODEM_LORA, TX_OUTPUT_POWER, 0, LORA_BANDWIDTH,
		LORA_SPREADING_FACTOR, LORA_CODINGRATE,
		LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
		true, 0, 0, LORA_IQ_INVERSION_ON, 3000 );

  Radio.SetRxConfig( MODEM_LORA, LORA_BANDWIDTH,
  	LORA_SPREADING_FACTOR, LORA_CODINGRATE, 0,
		LORA_PREAMBLE_LENGTH, LORA_SYMBOL_TIMEOUT,
		LORA_FIX_LENGTH_PAYLOAD_ON, 0, true, 0, 0,
		LORA_IQ_INVERSION_ON, true );

  /* create timer for the ubx */
	lora_pin_timer = OS_TIMER_CREATE("ubxrx", portCONVERT_MS_2_TICKS(1200),
		OS_TIMER_FAIL, (void *) OS_GET_CURRENT_TASK(), ubx_rx_cb);

	OS_ASSERT(lora_pin_timer);

	/* create timer for lora tx */
	lora_tx_timer = OS_TIMER_CREATE("loratx", portCONVERT_MS_2_TICKS(100),
		OS_TIMER_FAIL, (void *) OS_GET_CURRENT_TASK(), lora_tx_cb);

	OS_ASSERT(lora_tx_timer);

	/* create timer for lora rx */
	lora_rx_timer = OS_TIMER_CREATE("lorarx", portCONVERT_MS_2_TICKS(5000),
		OS_TIMER_FAIL, (void *) OS_GET_CURRENT_TASK(), lora_rx_cb);

	OS_ASSERT(lora_rx_timer);

	/* create timer for lora ack */
	lora_ack_timer = OS_TIMER_CREATE("loraack", portCONVERT_MS_2_TICKS(1000),
		OS_TIMER_FAIL, (void *) OS_GET_CURRENT_TASK(), lora_ack_cb);

	OS_ASSERT(lora_ack_timer);
}

#ifdef HELLO
static void
say_hi(osjob_t *job)
{
	PRIVILEGED_DATA static int	n;
	char				buf[64];

	os_setTimedCallback(job, os_getTime() + sec2osticks(1), say_hi);
	printf("Hello #%u @ %u (%ld)\r\n", n++, os_getTimeSecs(), os_getTime());
}
#endif

void
lora_task_notify_event(uint32_t notify)
{
	OS_TASK_NOTIFY(lora_task_handle, notify, eSetBits);
}

static void lora_do_send(bal_cmd_t cmd)
{
	if(cmd == BAL_CMD_NONE)
	{
		BufferSize = BAL_HDR_SIZE + 1;
		memcpy(&Buffer[BufferSize], (uint8_t *)&test_tx_counter, sizeof(test_tx_counter));
		BufferSize += sizeof(test_tx_counter);
		BufferSize += gps_read((char *)&Buffer[BufferSize], sizeof(Buffer)-BufferSize);
		test_tx_counter++;
		param_set(PARAM_COUNT, (uint8_t *)&test_tx_counter, sizeof(test_tx_counter));
	}
#ifdef DEBUG
	for (int i = 0; i < BufferSize; i++)
		printf(" %02x", Buffer[i]);
	printf("\r\n");
#endif
	Radio.Send( Buffer, BufferSize );
	if(lora_tx_counter == 1){
		led_notify(LED_STATE_TEST);
	}else{
		OS_TIMER_START(lora_tx_timer, OS_TIMER_FOREVER);
	}
}

static void lora_send_cmd_on_all(bal_cmd_t cmd)
{
	static bal_test_t send_cmd = BAL_TEST_NONE;
	send_cmd++;
	OS_DELAY_MS(1000);
	switch(send_cmd)
	{
	case BAL_TEST_7_7_125:
		lora_send(7, 7, 0, 20, 1000, 1, cmd);
		break;
	case BAL_TEST_7_12_125:
		lora_send(7, 12, 0, 20, 1000, 1, cmd);
		break;
	case BAL_TEST_7_7_500:
		lora_send(7, 7, 2, 20, 1000, 1, cmd);
		break;
	case BAL_TEST_7_12_500:
		lora_send(7, 12, 2, 20, 1000, 1, cmd);
		break;
	case BAL_TEST_NONE:
	default:
		send_cmd = BAL_TEST_NONE;
		break;
	}
	OS_DELAY_MS(1000);
}

static void lora_rx_cur_test(void)
{
	switch(cur_test)
	{
	case BAL_TEST_7_7_125:
		lora_receive(7, 7, 0, false);
		break;
	case BAL_TEST_7_12_125:
		lora_receive(7, 12, 0, false);
		break;
	case BAL_TEST_7_7_500:
		lora_receive(7, 7, 2, false);
		break;
	case BAL_TEST_7_12_500:
		lora_receive(7, 12, 2, false);
		break;
	case BAL_TEST_NONE:
	default:
		lora_receive(7, 12, 1, false);
		break;
		}
}

void
lora_task_func(void *param)
{
	int8_t wdog_id = sys_watchdog_register(false);

#ifdef HELLO
	osjob_t	hello_job;
#endif

#ifdef USE_MY_TIMER
	const timer0_config cfg = {
	    .clk_src = HW_TIMER0_CLK_SRC_FAST,
	    .fast_clk_div = HW_TIMER0_FAST_CLK_DIV_1,
	    .on_clock_div = false,
	    .on_reload = 0,
	    .t0_reload_m = 0xFFFF,
	    .t0_reload_n = 0
	};

	timer0_counter = 0;
	hw_timer0_init(NULL);
	hw_timer0_configure(&cfg);
	hw_timer0_register_int(timer0_int_handler);
	hw_timer0_enable();
#endif

	(void)param;
	param_init();
	led_notify(LED_STATE_BOOTING);
#ifdef HELLO
	os_setCallback(&hello_job, say_hi);
#endif
	led_notify(LED_STATE_IDLE);

	lora_task_handle = OS_GET_CURRENT_TASK();

	SX1276IoInit();
	// Radio initialization
	RadioEvents.TxDone = OnTxDone;
	RadioEvents.RxDone = OnRxDone;

	Radio.Init( &RadioEvents );

	param_get(PARAM_DEV_ID, &dev_id, sizeof(dev_id));
	param_get(PARAM_MODE, &lora_test_mode, sizeof(lora_test_mode));
	param_get(PARAM_TEST, &cur_test, sizeof(cur_test));

	switch(lora_test_mode){
		case BAL_MODE_RX:
			logger_init();
			lora_task_notify_event(EV_NOTIF_BTN_PRESS);
			break;
		case BAL_MODE_TX:
			gps_init();
			param_get(PARAM_PACKET_NO, (uint8_t *)&packet_no_to_send, sizeof(packet_no_to_send));
			param_get(PARAM_COUNT, (uint8_t *)&test_tx_counter, sizeof(test_tx_counter));
			break;
		default:
			break;
	}
	for (;;) {
		OS_BASE_TYPE ret;
		uint32_t notif;
		int tx_type = 0;

		/* notify watchdog on each loop */
		sys_watchdog_notify(wdog_id);

		/* suspend watchdog while blocking on OS_TASK_NOTIFY_WAIT() */
		sys_watchdog_suspend(wdog_id);

		/*
		 * Wait on any of the notification bits, then clear them all
		 */
		ret = OS_TASK_NOTIFY_WAIT(0, OS_TASK_NOTIFY_ALL_BITS, &notif, OS_TASK_NOTIFY_FOREVER);
		/* Blocks forever waiting for task notification. The return value must be OS_OK */
		OS_ASSERT(ret == OS_OK);

		/* resume watchdog */
		sys_watchdog_notify_and_resume(wdog_id);

		if(notif & EV_NOTIF_LORA_DIO)
		{
			gp_irqHandlers[0]();
		}
		if(notif & EV_NOTIF_BTN_PRESS)
		{
			button_press(rtc_get());
			switch(lora_test_mode)
			{
				case BAL_MODE_RX:
					lora_rx_cur_test();
					break;
				case BAL_MODE_TX:
					tx_type = iox_getpins();
					printf("iox: %x\r\n", tx_type);
					switch(tx_type)
					{
						case 0x8000:
							cur_test = BAL_TEST_7_7_125;
							lora_send_cmd_on_all(BAL_CMD_LORA_RX_7_7_125);
							break;
						case 0x4000:
							lora_send(7, 7, 0, 20, 7000, packet_no_to_send, 0);
							break;
						case 0x2000:
							cur_test = BAL_TEST_7_12_125;
							lora_send_cmd_on_all(BAL_CMD_LORA_RX_7_12_125);
							break;
						case 0x1000:
							lora_send(7, 12, 0, 20, 90000, packet_no_to_send, 0);
							break;
						case 0x0800:
							cur_test = BAL_TEST_7_7_500;
							lora_send_cmd_on_all(BAL_CMD_LORA_RX_7_7_500);
							break;
						case 0x0400:
							lora_send(7, 7, 2, 20, 2000, packet_no_to_send, 0);
							break;
						case 0x0200:
							cur_test = BAL_TEST_7_12_500;
							lora_send_cmd_on_all(BAL_CMD_LORA_RX_7_12_500);
							break;
						case 0x0100:
							lora_send(7, 12, 2, 20, 25000, packet_no_to_send, 0);
							break;
						default:
							break;
					}
					break;
				case BAL_MODE_ACK:
					tx_type = iox_getpins();
					switch(tx_type)
					{
						case 0x8000:
							cur_test = BAL_TEST_7_7_125;
							lora_rx_cur_test();
							break;
						case 0x2000:
							cur_test = BAL_TEST_7_12_125;
							lora_rx_cur_test();
							break;
						case 0x0800:
							cur_test = BAL_TEST_7_7_500;
							lora_rx_cur_test();
							break;
						case 0x0200:
							cur_test = BAL_TEST_7_12_500;
							lora_rx_cur_test();
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
		}
		if(notif & EV_NOTIF_CONS_RX)
		{
			cons_rx();
		}
		if(notif & EV_NOTIF_GPS_RX)
		{
			switch(lora_test_mode)
			{
				case BAL_MODE_RX:
					logger_add(&last_rx_data);
					if(last_rx_data.timemark_ms != 0){
						printf("%d,%d,%d.%02d,%ld,%ld,%ld,%ld\r\n", last_rx_data.counter, last_rx_data.rssi - 256, \
							(last_rx_data.snr >> 2), ((last_rx_data.snr & 0x03) * 25), \
							last_rx_data.timemark_ms, last_rx_data.timemark_ns, \
							last_rx_data.lat, last_rx_data.lon);
					}
					lora_rx_cur_test();
					break;
				case BAL_MODE_TX:
					gps_process();
					if(gps_data_ready() == 0)
					{
						lora_do_send(BAL_CMD_NONE);
					}
					break;
				default:
					break;
			}
		}
		if(notif & EV_NOTIF_LORA_TX)
		{
			if(lora_test_mode == BAL_MODE_TX){
				gps_prepare();
			}else{
				lora_do_send(BAL_CMD_NONE);
			}
		}
		if(notif & EV_NOTIF_LORA_RX)
		{
			if(lora_test_mode == BAL_MODE_RX){
				lora_rx_cur_test();
			}else{
				Radio.Rx(0);
			}
		}
		if(notif & EV_NOTIF_LORA_ACK)
		{
			if(lora_test_mode == BAL_MODE_RX){
				switch(cur_test)
				{
				case BAL_TEST_7_7_125:
					lora_send(7, 7, 0, 20, 100, 1, BAL_CMD_LORA_ACK);
					break;
				case BAL_TEST_7_12_125:
					lora_send(7, 12, 0, 20, 100, 1, BAL_CMD_LORA_ACK);
					break;
				case BAL_TEST_7_7_500:
					lora_send(7, 7, 2, 20, 100, 1, BAL_CMD_LORA_ACK);
					break;
				case BAL_TEST_7_12_500:
					lora_send(7, 12, 2, 20, 100, 1, BAL_CMD_LORA_ACK);
					break;
				default:
					break;
				}
				OS_TIMER_START(lora_rx_timer, OS_TIMER_FOREVER);
			}
		}
	}
}

void lora_receive(uint8_t chan, uint8_t	sf, \
	uint8_t	bw, bool inverted)
{
	lora_tx_counter = 1;
	OS_TIMER_STOP(lora_tx_timer, OS_TIMER_FOREVER);

	Radio.Sleep();

	Radio.SetChannel( ChannelFreqs[chan] );

	if(lora_test_mode != BAL_MODE_RX){
		printf("rx %ld %d %d\r\n", ChannelFreqs[chan], sf, bw);
	}

	Radio.SetRxConfig( MODEM_LORA, bw, sf, LORA_CODINGRATE, \
		0, LORA_PREAMBLE_LENGTH, LORA_SYMBOL_TIMEOUT,
		LORA_FIX_LENGTH_PAYLOAD_ON, 0, true, 0, 0,
		LORA_IQ_INVERSION_ON, true );

	Radio.Rx( 0 ); // Continuous Rx
}

void lora_send(uint8_t chan, uint8_t	sf, \
	uint8_t	bw, int8_t txpow, uint16_t interval, \
	uint16_t n, uint8_t cmd)
{
	lora_tx_counter = n;

	if(n != 1){
		OS_TIMER_CHANGE_PERIOD(lora_tx_timer, \
			portCONVERT_MS_2_TICKS(interval), OS_TIMER_FOREVER);
	}

	Radio.Sleep();

	Radio.SetChannel( ChannelFreqs[chan] );

	if(lora_test_mode != BAL_MODE_RX){
		printf("tx %ld %d %d\r\n", ChannelFreqs[chan], sf, bw);
	}

	Radio.SetTxConfig( MODEM_LORA, txpow, 0, bw, sf, \
		LORA_CODINGRATE, LORA_PREAMBLE_LENGTH, LORA_FIX_LENGTH_PAYLOAD_ON,
		true, 0, 0, LORA_IQ_INVERSION_ON, 3000);

	if(cmd == BAL_CMD_NONE){
		memcpy(Buffer, (uint8_t *)&bal_dev_tx_hdr, sizeof(bal_dev_tx_hdr));
		BufferSize = BAL_HDR_SIZE;
		BufferSize += param_get(PARAM_DEV_ID, &Buffer[BufferSize], sizeof(Buffer));
		if(lora_test_mode == BAL_MODE_TX){
			gps_prepare();
		}else{
			lora_do_send(BAL_CMD_NONE);
		}
	}else{
		BufferSize = bal_lora_cmd_get(cmd);
		lora_do_send(cmd);
	}
}
void lora_stop(void)
{
	lora_tx_counter = 1;
	OS_TIMER_STOP(lora_tx_timer, OS_TIMER_FOREVER);
	printf("LoRa Stop\r\n");
	Radio.Sleep();
	test_tx_counter = 0;
	param_set(PARAM_COUNT, (uint8_t *)&test_tx_counter, sizeof(test_tx_counter));
}

void OnRxDone( uint8_t *payload, uint16_t size, int16_t rssi, int8_t snr )
{
	if(size > 0){
		bool is_bal_cmd = false;
		bal_cmd_t cmd;

		memset(&last_rx_data, 0x00, sizeof(last_rx_data));

		led_notify(LED_STATE_RX);

		if(memcmp(payload, bal_cmd_hdr, sizeof(bal_cmd_hdr)) == 0){
			is_bal_cmd = true;
		}

		if((size > 0) && (is_bal_cmd)){
			cmd = *(payload + sizeof(bal_cmd_hdr));

			if((lora_test_mode != BAL_MODE_ACK) && (size == BAL_HDR_SIZE + 1))
			{
				uint16_t delay = dev_id * 3000;
				Radio.Sleep();
				switch(cmd){
				case BAL_CMD_LORA_RESET:
					hw_cpm_reboot_system();
					break;
				case BAL_CMD_LORA_RX:
					Radio.Rx( 0 ); // Continuous Rx
					break;
				case BAL_CMD_LORA_TX:
	//				lora_do_send(BAL_CMD_NONE);
					break;
				case BAL_CMD_LORA_RX_7_7_125:
					cur_test = BAL_TEST_7_7_125;
					param_set(PARAM_TEST, &cur_test, sizeof(cur_test));
					OS_TIMER_CHANGE_PERIOD(lora_ack_timer, OS_MS_2_TICKS(delay), OS_TIMER_FOREVER);
					OS_TIMER_START(lora_ack_timer, OS_TIMER_FOREVER);
					break;
				case BAL_CMD_LORA_RX_7_12_125:
					cur_test = BAL_TEST_7_12_125;
					param_set(PARAM_TEST, &cur_test, sizeof(cur_test));
					OS_TIMER_CHANGE_PERIOD(lora_ack_timer, OS_MS_2_TICKS(delay), OS_TIMER_FOREVER);
					OS_TIMER_START(lora_ack_timer, OS_TIMER_FOREVER);
					break;
				case BAL_CMD_LORA_RX_7_7_500:
					cur_test = BAL_TEST_7_7_500;
					param_set(PARAM_TEST, &cur_test, sizeof(cur_test));
					OS_TIMER_CHANGE_PERIOD(lora_ack_timer, OS_MS_2_TICKS(delay), OS_TIMER_FOREVER);
					OS_TIMER_START(lora_ack_timer, OS_TIMER_FOREVER);
					break;
				case BAL_CMD_LORA_RX_7_12_500:
					cur_test = BAL_TEST_7_12_500;
					param_set(PARAM_TEST, &cur_test, sizeof(cur_test));
					OS_TIMER_CHANGE_PERIOD(lora_ack_timer, OS_MS_2_TICKS(delay), OS_TIMER_FOREVER);
					OS_TIMER_START(lora_ack_timer, OS_TIMER_FOREVER);
					break;
				default:
					hw_cpm_reboot_system();
					break;
				}
			}else{
				if(cmd == BAL_CMD_LORA_ACK){
					if((memcmp((payload + BAL_HDR_SIZE + 1), bal_dev_rx_hdr, sizeof(bal_dev_rx_hdr)) == 0)){
						if(payload[1 + 2 * BAL_HDR_SIZE] < 5){
							ack_count |= 1 << payload[1 + 2 * BAL_HDR_SIZE];
						}
					}
					if(ack_count == 0x1E){
						led_notify(LED_STATE_SENDING);
					}
				}
				printf("ACK %d from: ", ack_count);
				for (int i = 0; i < size; i++)
						printf(" %02x", payload[i]);
				printf("\r\n");
			}
		}else{
			last_rx_data.rssi = (rssi & 0x00FF);
			last_rx_data.snr = snr;
			if((memcmp(payload ,bal_dev_tx_hdr ,sizeof(bal_dev_tx_hdr)) == 0) \
				&& (size >= 3) && (lora_test_mode == BAL_MODE_RX)){
				if(size >= 5)
				{
					last_rx_data.counter = (payload[4] << 8) + payload[3];
				}
				if(size >= 16)
				{
					last_rx_data.lat = (payload[9] << 24) + (payload[8] << 16) \
						+ (payload[7] << 8) + payload[6];
					last_rx_data.lon = (payload[13] << 24) + (payload[12] << 16) \
						+ (payload[11] << 8) + payload[10];
				}
				OS_TIMER_START(lora_pin_timer, OS_TIMER_FOREVER);
			}else{
				printf("rssi %d, snr %d.%02d, data:", rssi, (snr >> 2), ((snr & 0x03) * 25));
				for (int i = 0; i < size; i++)
						printf(" %02x", payload[i]);
				printf("\r\n");
				if(lora_test_mode == BAL_MODE_ACK)
				{
					OS_TIMER_START(lora_rx_timer, OS_TIMER_FOREVER);
				}else{
					hw_cpm_reboot_system();
				}
			}
		}
	}
}

void OnTxDone( void )
{
	led_notify(LED_STATE_TX);
	Radio.Sleep( );
}

#ifdef USE_MY_TIMER
uint32_t lora_get_timestamp(void)
{
    uint16_t temp = 0xFFFF - hw_timer0_get_t0();
    return ((timer0_counter << 16) + temp);
}
#endif
