/*
 * logger.h
 *
 *  Created on: 7 Jul 2019
 *      Author: balog
 */

#ifndef LORA_LOGGER_H_
#define LORA_LOGGER_H_

typedef struct{
		uint16_t 	counter;
		uint8_t		rssi;
		int8_t		snr;
		uint32_t 	timemark_ms;
		uint32_t	timemark_ns;
		int32_t		lat;	/* Positive: North */
		int32_t		lon;	/* Positive: East */
}__attribute__((packed)) logger_data_t;

void logger_init(void);

void logger_add(logger_data_t *data);

void logger_save(uint16_t addr);

void logger_print(uint16_t start, uint16_t end);

#endif /* LORA_LOGGER_H_ */
